const { readFileSync, writeFileSync } = require('fs')
const { resolve } = require('path')
const convert = require('xml-js')
const svg2ttf = require('svg2ttf')
const ttf2woff2 = require('ttf2woff2')
const findUsedIcons = require('./find-used-icons.js')

const usedIcons = findUsedIcons()

console.log('create minimal svg font')
const includedIcons = []
const sourceSvgFile = convert.xml2js(readFileSync(resolve(__dirname, '../themes/anoxinonmedia/static/fonts/forkawesome-webfont.svg')), { compact: false })
sourceSvgFile.elements.filter((element) => element.type === 'element').forEach((element) => {
  element.elements.filter((element) => element.type === 'element').forEach((element) => {
    element.elements.filter((element) => element.type === 'element').forEach((element) => {
      element.elements = element.elements.filter((element) => {
        const isGlyph = element.type === 'element' && element.name === 'glyph'
        
        if (isGlyph) {
          const isUsed = usedIcons.indexOf('fa-' + element.attributes['glyph-name']) !== -1
          
          if (isUsed) { includedIcons.push('fa-' + element.attributes['glyph-name']) }
          
          return isUsed
        } else {
          return true
        }
      })
    })
  })
})
const svgString = convert.js2xml(sourceSvgFile, { compact: false })

console.log('convert to ttf')
const ttf = svg2ttf(svgString, { ts: 0 })

console.log('convert to woff2 file')
const woff2 = ttf2woff2(Buffer.from(ttf.toArray()))

console.log('save new woff2 file')
writeFileSync(resolve(__dirname, '../themes/anoxinonmedia/static/fonts/forkawesome-webfont.woff2'), woff2)

console.log('check for missing icons')
usedIcons.forEach((usedIcon) => {
  if (includedIcons.indexOf(usedIcon) === -1) {
    console.log('warning: used ' + usedIcon + ' which does not exist')
  }
})

console.log('done')
