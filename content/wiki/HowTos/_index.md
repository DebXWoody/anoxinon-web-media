---
title: HowTos, Tipps & Tricks
layout: wikipage
---

Kennst du es auch: Schon wieder diesen einen Foren-Thread aufgerufen, schon wieder die drei Manpages durchgeblättert und schon wieder diese vier Blogposts gelesen, um diese eine scheinbar triviale Aufgabe zu lösen. Würde man sich doch nur die Mühe machen, das aufzuschreiben...
Wenn du das kennst, bist du hier richtig. Denn wir kennen das auch - und zumindest hin und wieder machen wir uns die Mühe und schreiben die Tipps nieder, die uns das Arbeiten erleichtern.