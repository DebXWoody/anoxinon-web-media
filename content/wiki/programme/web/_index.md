---
title: Web
layout: wikipage
--- 

## Browser

- [Firefox (Windows, MacOS, Linux, Android, iOS)](https://mozilla.org/en-US/firefox/)
- [FOSS Browser (Android)](https://f-droid.org/en/packages/de.baumann.browser/)  

## FTP

- [FileZilla (Windows, MacOS, Linux)](https://filezilla-project.org/download.php)  
  
