---
title: Backups
layout: wikipage
--- 

## Snapshots

- [Timeshift (Linux)](https://github.com/teejee2008/timeshift)
- [Snapper (Linux)](https://en.opensuse.org/Portal:Snapper)

## Systemabbilder

- [Clonezilla (Linux, Livesystem)](https://clonezilla.org)  

## Datenbackups

### Grafische Programme

- [Back In Time (Linux)](https://github.com/bit-team/backintime#back-in-time)
- [Vorta - Client für Borg Backup (Windows, MacOS, Linux)](https://vorta.borgbase.com/)

### Kommandozeilen-Programme (CLI/TUI)

- [Rsync (Linux)](https://wiki.ubuntuusers.de/rsync)
- [Restic (Windows, MacOS, Linux)](https://github.com/restic)  


