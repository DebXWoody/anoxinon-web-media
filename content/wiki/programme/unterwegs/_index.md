---
title: Unterwegs
layout: wikipage
--- 

## ÖPVN

- [Transportr (Android)](https://f-droid.org/packages/de.grobox.liberario/)  

## Reisekostenverwaltung

- [Tricky Tripper (Android)](https://f-droid.org/en/packages/de.koelle.christian.trickytripper/)  

## Karte und Navigation

- [OsmAnd (Android)](https://f-droid.org/en/packages/net.osmand.plus)  
