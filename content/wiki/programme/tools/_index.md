---
title: Tools
layout: wikipage
--- 

## Bildschirmfotos

- [Kazam (Linux)](https://launchpad.net/kazam)
- [Flameshot (Linux)](https://flameshot.js.org)
- [Grim (Linux)](https://github.com/emersion/grim)  

## E-Book Reader

- [Calibre (Windows, MacOS, Linux)](https://calibre-ebook.com)   

## Passwortmanager

- [KeePassXC (Windows, MacOS, Linux)](https://keepassxc.org)    

## Remote Desktop

- [Remmina (Linux)](https://remmina.org)  

## Virtuelle Maschinen

- [GNOME Boxes (Linux)](https://wiki.gnome.org/Apps/Boxes)
- [VirtualBox (Windows, MacOS, Linux)](https://virtualbox.org)  

## Verschlüsselung

- [VeraCrypt (Windows, MacOS, Linux)](https://veracrypt.fr)  

## Automation

- [Autokey (Linux)](https://github.com/autokey/autokey)  

## Textvergleich   

- [Meld (Windows, Linux)](https://meldmerge.org)  
