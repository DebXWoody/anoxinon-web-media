---
title: Betriebssysteme
layout: wikipage
---   

**Betriebssysteme** oder auch **OS** (für **O**perating **S**ystem(s)) bilden die Basis für unsere digitalen Geräte. Sie sorgen dafür, das Programme lauffähig sind und auf die Hardware des Geräts zugreifen können. Damit ist zum Beispiel eine Ausgabe auf dem Bildschirm oder das Verarbeiten von einem Fingerdruck auf dem Display möglich. Bekannte Vertreter sind Windows oder MacOS, die man traditionell von Desktop-PCs oder Notebooks kennt und Android sowie iOS für Smartphones. Daneben gibt es allerdings zum Beispiel auch **Linux**, was vor allem hinsichtlich Datenschutz und Sicherheit interessante Aspekte bietet.

## Einstieg

[**Flyer - Gründe für Linux**](/files/Flyer_Linux_final_Webseite.pdf): Eine kurze Übersicht, über die Merkmale, die Linux auszeichnen.

[**Windows 10 Telemetrie (Bund)**](https://www.bsi.bund.de/SharedDocs/Downloads/DE/BSI/Cyber-Sicherheit/SiSyPHus/Analyse_Telemetriekomponente_1_2.html):  Analyse der Telemetriekomponente in Windows 10 - Konfigurations- und Protokollierungsempfehlung 
