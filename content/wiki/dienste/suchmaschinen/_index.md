---
title: Suchmaschinen
layout: wikipage
--- 

## Allgemein

- [Metager](https://metager.de) - Angebot des SUMA-EV, sogar mit Proxy-Seiten-Aufruf. Metager ist zudem direkt aus dem Tor-Netzwerk erreichbar ([Datenschutzerklärung](https://metager.de/datenschutz), [Verantwortliche:r](https://metager.de/impressum))
- [Qwant](https://qwant.com) - französische Suchmaschine mit umfassendem Suchangebot, wie einem [Kartendienst](https://qwant.com/maps) und einer [Kindersuchmaschine](https://www.qwantjunior.com/) ([Datenschutzerklärung](https://about.qwant.com/de/rechtlich/datenschutzrichtlinie/), [Verantwortliche:r](https://about.qwant.com/de/rechtlich/impressum/))

Oft werden noch DuckDuckGo und Ecosia genannt, welche wir jedoch nicht in das Ranking aufnehmen. DuckDuckGo hat seinen Standort in den USA und Ecosia nutzt standardmäßig einen Client-ID-Parameter, damit Microsoft die Suchergebnisse personalisieren kann. (Ecosia nutzt im Hintergund Microsofts Suchdienst Bing.)

## Searx

[Searx](https://searx.me) ist eine quelloffene Metasuchmaschine. Das bedeuet, sie hat keinen eigenen Index, sondern zapft die Ergebnisse anderer Suchmaschinen an.


- [Disroot Search](https://disroot.org/de/services/search) - Searx-Angebot von Disroot ([Datenschutzerklärung](https://disroot.org/de/privacy_policy), [Verantwortliche:r](https://disroot.org/de/privacy_policy))
- [Snopyta Search](https://search.snopyta.org/) - Searx-Angebot von Snopyta ([Datenschutzerklärung](https://snopyta.org/privacy_policy/), [Verantwortliche:r](https://snopyta.org/legal/))
- [Spot](https://spot.ecloud.global/) - Angepasste Searx-Instanz der gemeinnützigen e Foundation aus Frankreich ([Datenschutzerklärung](https://e.foundation/legal-notice-privacy/), [Verantwortliche:r](https://e.foundation/legal-notice-privacy/))
- [SP-Codes Searx](https://sp-codes.de/de/services/searx/) - Searx-Angebot von SP-Codes ([Datenschutzerklärung](https://sp-codes.de/de/privacy), [Verantwortliche:r](https://sp-codes.de/de/imprint/))
