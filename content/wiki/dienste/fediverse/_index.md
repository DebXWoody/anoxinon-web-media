---
title: Fediverse - ein dezentrales soziales Netzwerk
layout: wikipage
---

Das Fediverse ist ein föderiertes und dezentrales soziales Netzwerk. Das bedeutet, es besteht aus vielen verschiedenen Instanzen, die miteinander vernetzt sind. Als Nutzer:in hat man daher die Wahl, sich eine passende Instanz herauszusuchen. Nachfolgend listen wir ein paar Beispiele auf.

## Microblogging
  
**Instanz-Listen:**

Neben unserer Liste kann man weitere Instanzen unter folgenden Quellen finden:

- [Mastodon Communities](https://joinmastodon.org/communities) - Kategorisierte Instanz-Liste der Mastodon Entwickler:innen
- [Mastodon instances](https://instances.social/) - Interaktive Instanz-Auswahl

**Eine Auswahl:**

*  [Anoxinon Social](https://social.anoxinon.de)  - Unsere hauseigene Mastodon-Instanz ([Datenschutzerklärung](https://social.anoxinon.de/terms), [Verantwortliche:r](https://anoxinon.de/impressum/))
*  [bonn.social](https://bonn.social) - Mastodon-Instanz rund um Bonn ([Datenschutzerklärung](https://bonn.digital/datenschutz/), [Verantwortliche:r](https://bonn.digital/impressum))
*  [metalhead.club](https://metalhead.club)  - Eine Mastodon-Instanz für Metalheads ([Datenschutzerklärung](https://metalhead.club/terms), [Verantwortliche:r](https://uberspace.de/de/about/imprint/))
*  [social.tchncs.de](https://social.tchncs.de)  - Eine der ältesten und größten Mastodon-Instanzen ([Datenschutzerklärung](https://social.tchncs.de/terms), [Verantwortliche:r](https://tchncs.de/impressum))


