---
title: Onlinetelefonie & VoIP Anbieter:innen
layout: wikipage
---

## Videokonferenzen

### Jitis Meet

Für Videokonferenzen mit Freunden und Familie bietet sich [Jitsi Meet](https://jitsi.org/jitsi-meet/) an. Neben Videotelefonie kann es zum Beispiel auch zum Screensharing eingesetzt werden, Apps gibt es für Android und iOS, auf dem Desktop nutzt man es im Browser.

Mittlerweile gibt es Jitsi Meet Instanzen wie Sand am Meer und auch unzählige Listen, die diese aufführen:

- [Liste von den Jitsi Meet Entwickler:innen](https://jitsi.github.io/handbook/docs/community/community-instances#germany-a-idgermanya)
- [Favstarmafias Liste](https://fediverse.blog/~/DonsBlog/videochat-server)
- [Tobias Scheibles Liste](https://scheible.it/liste-mit-oeffentlichen-jitsi-meet-instanzen/)

Ein Auszug von Instanzen findet sich in folgender Auflistung.

- [Adminforge Meet](https://meet.adminforge.de/) - Jitsi Meet von Adminforge ([Datenschutzerklärung](https://adminforge.de/impressum/#jitsi-meet), [Verantwortliche:r](https://adminforge.de/impressum/))
- [Blabber Meet](https://meet.blabber.im) - Videokonferenzen vom Blabber Projekt, einem Adminforge-Service ([Datenschutzerklärung](https://adminforge.de/impressum/#jitsi-meet), [Verantwortliche:r](https://adminforge.de/impressum/))
- [Disroot Calls](https://disroot.org/de/services/calls) - Jitsi Meet-Instanz von Disroot ([Datenschutzerklärung](https://disroot.org/de/privacy_policy), [Verantwortliche:r](https://disroot.org/de/privacy_policy)) 
- [Golem Jitsi Meet](https://meet.golem.de/) - Jitsi Meet vom Deutschen IT-Magazin Golem ([Datenschutzerklärung](https://www.golem.de/sonstiges/Datenschutz.html), [Verantwortliche:r](https://www.golem.de/sonstiges/impressum.html))
- [Kuketz Meet](https://www.kuketz-meet.de/) - Jitsi Meet-Instanz vom Kuketz Blog ([Datenschutzerklärung](https://www.kuketz-blog.de/datenschutzhinweis-kuketz-meet-de/), [Verantwortliche:r](https://www.kuketz-blog.de/impressum/))
- [LibreOps Meet](https://meet.libreops.cc/) - LibreOps' Jitsi Meet-Angebot ([Datenschutzerklärung](https://libreops.cc/terms.html), [Verantwortliche:r](https://libreops.cc/about/))
- [mailbox.org video](https://mailbox.org/de/produkte#videokonferenz) - Für mailbox.org Kunden gibt es mailbox.org video auf Basis von Jitsi Meet ([Datenschutzerklärung](https://mailbox.org/de/datenschutzerklaerung), [Verantwortliche:r](https://mailbox.org/de/impressum))
- [Nitrokey's Jitsi Meet](https://meet.nitrokey.com/) - Jitsi Meet von Nitrokey, einem Deutschen Hersteller von offener Hardware ([Datenschutzerklärung](https://www.nitrokey.com/de/datenschutzerklaerung), [Verantwortliche:r](https://www.nitrokey.com/de/impressum))
- [Scheible Jitsi Meet](https://meet.scheible.it/) - Jitsi Meet von Tobias Scheible ([Datenschutzerklärung](https://scheible.it/datenschutzhinweis/), [Verantwortliche:r](https://scheible.it/impressum/))
- [Snopyta Talk](https://talk.snopyta.org/) - Jitsi Meet von Snopyta ([Datenschutzerklärung](https://snopyta.org/privacy_policy/), [Verantwortliche:r](https://snopyta.org/legal/))
- [SP-Codes Jitsi Meet](https://sp-codes.de/de/services/jitsi/) - SP-Codes' Instanz von Jitsi Meet ([Datenschutzerklärung](https://sp-codes.de/de/privacy), [Verantwortliche:r](https://sp-codes.de/de/imprint/))
- [Systemli Jitsi Meet](https://www.systemli.org/service/meet) - Jitsi Meet-Angebot von Systemli ([Datenschutzerklärung](https://www.systemli.org/tos/), [Verantwortliche:r](https://www.systemli.org/support-us/))


### BigBlueButton

Für Videokonferenzen, aber auch für Webinare oder Vorträge eignet sich [BigBlueButton](https://bigbluebutton.org).

- [Senfcall](https://senfcall.de/) - BigBlueButton-Instanz aus Deutschland von einer studentischen Initiative ([Datenschutzerklärung](https://senfcall.de/gdpr), [Verantwortliche:r](https://senfcall.de/imprint))

## Audiokonferenzen

### Mumble

[Mumble](https://mumble.info) ist eine Sprachkonferenzsoftware änhlich zu TeamSpeak. In privaten Räumen kann man sich austauschen. Mumble kann entweder selbst gehostet werden oder man sucht sich einen Server seines Vertrauens. Wer sich bei Mumble unterhalten möchte, muss sich auf demselben Server befinden, da man sich in Räumen trifft. 

[**Liste von Mumble-Instanzen**](https://mumble.com/serverlist/?_set[language]=da)

* [**Anoxinon Mumble**](https://anoxinon.de/dienste/mumble/) - Die hauseigene Mumble Instanz von uns [(Datenschutzerklärung, ]()[Verantwortliche:r)](https://anoxinon.de/impressum/)
* [Disroot Audio](https://disroot.org/de/services/audio) - Das Mumble-Angebot von Disroot ([Datenschutzerklärung](https://media.ccc.de/about.html#privacy)[Verantwortliche:r)](https://www.ccc.de/en/imprint) [(Datenschutzerklärung, ](https://disroot.org/de/privacy_policy), [Verantwortliche:r](https://disroot.org/de/privacy_policy))
* [Natenom Mumble](https://wiki.natenom.de/ueber/natemologie-zentrum) - Von Natenom betriebene Mumble-Instanz ([Datenschutzerklärung](https://wiki.natenom.de/sammelsurium/datenschutz/mumble-server), [Verantwortliche:r](https://www.natenom.com/impressum/))
* [LibreOps Mumble](https://libreops.cc/mumble.html) - Mumble betrieben von LibreOps ([Datenschutzerklärung](https://libreops.cc/terms.html), [Verantwortliche:r](https://libreops.cc/about/))

Mumble-Apps gibt es für Windows, Linux, Mac und Android. Eine Auswahl findet sich [in unserem Wiki unter Programme -> Kommunikation -> VoIP](/wiki/programme/kommunikation/)
