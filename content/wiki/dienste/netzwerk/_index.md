---
title: Netzwerk
layout: wikipage
---

## DNS

- [Digitale Gesellschaft DNS](https://www.digitale-gesellschaft.ch/dns/) - DNS-Instanz des Schweizer Vereins Digitale Gesellschaft ([Datenschutzerklärung](https://dns.digitale-gesellschaft.ch/), [Verantwortliche:r](https://www.digitale-gesellschaft.ch/uber-uns/))
- [Dismail DNS](https://dismail.de/info.html#dns) - DNS-Resolver von Dismail, mit Werbeblocker ([Datenschutzerklärung](https://dismail.de/datenschutz.html), [Verantwortliche:r](https://dismail.de/impressum.html))
- [DNSforge](https://dnsforge.de/) - DNS-Instanz von Adminforge, mit Werbeblocker ([Datenschutzerklärung](https://dnsforge.de/), [Verantwortliche:r](https://adminforge.de/impressum/))
- [LibreDNS](https://libredns.gr/) - DNS-Angebot von LibreOps ([Datenschutzerklärung](https://libredns.gr/), [Verantwortliche:r](https://libreops.cc/about/))
- [RadicalDNS](https://libreops.cc/radicaldns.html) - DNS-Resolver von LibreOps ([Datenschutzerklärung](https://libreops.cc/terms.html), [Verantwortliche:r](https://libreops.cc/about/))

