---
title: XMPP Anbieter:innen
layout: wikipage
---

## Liste von XMPP-Anbieter:innen

Inzwischen hat das XMPP-Ökosystem doch eine ansehliche Größe erreicht und umfasst viele duzend Instanzen. Einige empfehlenswerte listen wir nachfolgend auf.

- [Anoxinon Messenger](https://anoxinon.de) - Die hauseigene XMPP-Instanz des Anoxinon e.V. ([Datenschutzerklärung](https://anoxinon.de/datenschutzerklaerung_xmpp/), [Verantwortliche:r](https://anoxinon.de/impressum/))
- [Dismail XMPP](https://dismail.de) - XMPP-Angebot von Dismail ([Datenschutzerklärung](https://dismail.de/datenschutz.html), [Verantwortliche:r](https://dismail.de/impressum.html))
- [Libreops XMPP](https://libreops.cc/) - Libreops' XMPP-Instanz ([Datenschutzerklärung](https://libreops.cc/terms.html), [Verantwortliche:r](https://libreops.cc/about/))
- [mailbox.org XMPP](https://mailbox.org) - Wer ein bezahltes E-Mail-Konto bei mailbox.org hat, bekommt automatisch auch ein XMPP-Konto zur E-Mail-Adresse ([Datenschutzerklärung](https://mailbox.org/de/datenschutzerklaerung), [Verantwortliche:r](https://mailbox.org/de/impressum))
- [Snopyta Jabber/XMPP](https://snopyta.org) - XMPP-Instanz von Snopyta ([Datenschutzerklärung](https://snopyta.org/privacy_policy/), [Verantwortliche:r](https://snopyta.org/legal/))
- [trashserver.net XMPP](https://trashserver.net) - Trashserver XMPP-Projekt ([Datenschutzerklärung](https://trashserver.net/datenschutz/), [Verantwortliche:r](https://trashserver.net/impressum/))
- [Wiuwiu XMPP](https://wiuwiu.de) - XMPP-Service, der neben dem kostenlosen Angebot auch ein bezahltes Abo mit eigenen Domains bietet ([Datenschutzerklärung](https://wiuwiu.de/Privacy/), [Verantwortliche:r](https://wiuwiu.de/Imprint/))

Es gibt zahlreiche [XMPP-Apps, einige davon haben wir in unserem Wiki gelistet](/wiki/programme/kommunikation/) Und wer mit XMPP noch alleine ist, kann zumindest einen der zahlreichen MUCs (Multi-User-Chats) beitreten. Für einen [Schellstart mit XMPP unter Android empfiehlt sich unsere Wiki-Seite eigens dazu](/wiki/programme/kommunikation/conversations/), dort findet sich auch ein übersichtlicher [Flyer](/files/Anleitung_Conversations.pdf), der viele wichtige Funktionen erklärt.

## Instanzlisten

Neben den Vorschlägen von uns gibt es noch weitere Auflistungen von XMPP-Instanzen:

- [Freie Messenger empfehlenswerte Server](https://www.freie-messenger.de/sys_xmpp/server/#empfehlenswerte-server) - Vom Freie Messenger Projekt empfohlene XMPP-Anbieter:innen
- [Öffentliche XMPP-Server bei jabber.at](https://list.jabber.at) - Auflistung von XMPP-Instanzen, die von jabber.at gepflegt wird
- [XMPP Provider Recommendations](https://wiki.xmpp.org/web/Provider_Recommendations) - XMPP-Instanz-Empfehlungen im Wiki der XMPP Standards Foundation


## Öffentliche Gruppenchats

Im XMPP-Universum gibt es unzählige öffentliche Gruppenchats, denen man beiwohnen und sich mit (meist ;-)) freundlichen Leuten austauschen kann. Ein paar finden sich in der Liste unten.

- [XMPP Chatraum-Suche](https://search.jabber.network/) - Suchmaschine für öffentliche XMPP-Gruppenchats
- [Anoxinon e.V. Gruppenchat](xmpp:anoxinon@conference.anoxinon.me?join) - Offizieller XMPP-Gruppenchat des Anoxinon e.V. rund um Datenschuz, open source und mehr
 
