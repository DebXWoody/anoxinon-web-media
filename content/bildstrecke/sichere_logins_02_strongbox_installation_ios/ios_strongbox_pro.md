---
title: Strongbox einrichten  
description: Zurück in Strongbox werden wir gefragt, ob wir Strongbox Pro testen möchten. Das benötigen wir an der Stelle nicht und wählen "Später fragen". 
image: /img/sichere_logins_02/ios_strongbox_firststart_09_e.PNG
weight: 18
---
