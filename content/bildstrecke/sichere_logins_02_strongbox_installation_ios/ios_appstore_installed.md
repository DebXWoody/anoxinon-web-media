---
title: Strongbox ist installiert 
description: Fertig - Strongbox ist installiert! Nun fahre mit der Einrichtung fort, klicke dazu auf "Öffnen" und starte Strongbox. 
image: /img/sichere_logins_02/ios_appstore_strongbox_installed_e.png
weight: 7
---
