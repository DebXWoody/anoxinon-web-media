---
title: Strongbox entsperren 
description: Wenn Du die Passwortdatei das erste Mal öffnest, kannst Du noch einige Dinge einstellen. Gehe dazu weiter mit "Ok, legen wir los...". 
image: /img/sichere_logins_02/ios_strongbox_firststart_17_e.PNG
weight: 27
---
