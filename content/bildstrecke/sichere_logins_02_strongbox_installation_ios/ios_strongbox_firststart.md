---
title: Strongbox einrichten  
description: Beim ersten Start von Strongbox musst Du Dich nun durch den Einrichtungsassistenten klicken. Los geht es mit dem gleichnamigen Knopf "Los geht's" ;-)
image: /img/sichere_logins_02/ios_strongbox_firststart_01_e.png
weight: 8
---
