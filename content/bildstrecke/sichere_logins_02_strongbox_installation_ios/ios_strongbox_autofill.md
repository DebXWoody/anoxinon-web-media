---
title: Strongbox einrichten  
description: Anschließend darfst Du AutoFill für Strongbox einrichten, dazu zeigt Dir die App eine Anleitung. Mit Hilfe von Autofill kann Strongbox Deine Zugangsdaten in Apps automatisch ausfüllen. Die Schritte dafür werden wir in den nachfolgenden Bildschirmfotos ebenfalls zeigen.
image: /img/sichere_logins_02/ios_strongbox_firststart_03_e.PNG 
weight: 11
---
