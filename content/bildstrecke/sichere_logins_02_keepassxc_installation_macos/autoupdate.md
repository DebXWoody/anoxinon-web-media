---
title: KeePassXC einrichten 
description: An der Stelle kannst Du einstellen, ob KeePassXC automatisch nach Aktualisierungen suchen soll. Das ist zu empfehlen. 
image: /img/sichere_logins_02/08_macos_keepassxc_updatecheck.png
weight: 8
---
