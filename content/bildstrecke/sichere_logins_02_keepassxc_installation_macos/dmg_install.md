---
title: KeePassXC installieren 
description: Nun kommt es zum wichtigen Part. Du musst KeePassXC installieren, indem Du das KeePassXC-Logo mit der Maus packst und festhälst und in den Applications-Ordner verschiebst. 
image: /img/sichere_logins_02/05_macos_keepassxc_app_copy.png
weight: 5
---
