---
title: KeePassXC einrichten 
description: Öffne nun KeePassXC auf dem Weg Deiner Wahl, zum Beispiel über die Spotlight-Suche, die Du mit der Tastenkombination Option + Leertaste aufrufen kannst. 
image: /img/sichere_logins_02/06_macos_keepassxc_search.png
weight: 6
---
