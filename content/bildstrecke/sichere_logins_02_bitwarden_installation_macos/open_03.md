---
title: Bitwarden installieren
description: Mit "Öffnen" gelangt man das erste man in das frische Bitwarden-Programm.
image: /img/sichere_logins_02/06_macos_bitwarden_appstore_oeffnen.png
weight: 6
---
