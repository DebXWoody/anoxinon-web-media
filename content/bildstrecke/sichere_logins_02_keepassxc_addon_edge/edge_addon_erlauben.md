---
title: KeePassXC für Edge installieren 
description: Edge macht Dich noch auf die Berechtigungen, die KeePassXC benötigt aufmerksam. Nach einem weiteren Klick auf "Erweiterung hinzufügen" ist das Addon installiert.
image: /img/sichere_logins_02/edge_keepassxc_06_e.png
weight: 6
---
