---
title: KeePassXC-Addon mit KeePassXC verbinden 
description: Nun kannst Du zurück im Edg das kleine, neu hinzugekommene KeePassXC-Symbol in der oberen Leiste wählen. Wenn Du Edge in der Zwischenzeit nicht neugestartet hast, musst Du dort auf "Neu laden" klicken, damit das Addon Dein KeePassXC-Programm erkennen kann. Beachte allerdings, dass Dein KeePassXC-Passwortspeicher dazu entsperrt sein muss - sonst passiert nichts!!! (Standardmäßig sperrt sich KeePassXC nach einer Zeit von selbst wieder.) Wenn nichts passiert - das gilt auch für die folgenden Schritte - dann öffne das Addon einmal neu! 
image: /img/sichere_logins_02/edge_keepassxc_10_e.png
weight: 10
---
