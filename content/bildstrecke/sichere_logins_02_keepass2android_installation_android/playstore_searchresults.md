---
title: KeePass2Android im PlayStore suchen
description: KeePass2Android wird Dir direkt als erstes Suchergebnis präsentiert, klicke "Installieren", um Dir die App auf's Handy zu laden. 
image: /img/sichere_logins_02/04_android_keepass2android_e.png
weight: 4
---
