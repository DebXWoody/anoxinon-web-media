---
title: KeePass2Android im PlayStore suchen
description: Als erstes musst Du den PlayStore öffnen, dieser findet sich als Symbol auf Deinem Android-Homescreen. Suche ihn und klicke ihn an. 
image: /img/sichere_logins_02/01_android_keepass2android_e.PNG
weight: 1
---
