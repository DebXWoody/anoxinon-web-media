---
title: KeePassXC für Firefox suchen 
description: Nun wird Dir das Addon schon ganz oben vorgeschlagen, es trägt den Titel "KeePassXC-Browser". Um es installieren zu können, klicke es an.
image: /img/sichere_logins_02/mozilla_addons_keepassxc_suche_e.png
weight: 3
---
