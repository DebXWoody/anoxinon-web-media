---
title: KeePassXC-Addon mit KeePassXC verbinden 
description: Jetzt hast Du die Möglichkeit, das Addon mittels Klick auf "Verbinden" mit Deinem KeePassXC-Programm zu verbinden.
image: /img/sichere_logins_02/keepassxc_firefox_connect_e.png
weight: 11
---
