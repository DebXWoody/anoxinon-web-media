---
title: KeePassXC für Firefox installieren 
description: Firefox macht Dich noch auf die Berechtigungen, die KeePassXC benötigt aufmerksam. Nach einem weiteren Klick auf "Hinzufügen" ist das Addon installiert.
image: /img/sichere_logins_02/mozilla_addons_keepassxc_install_dialog_e.png
weight: 5
---
