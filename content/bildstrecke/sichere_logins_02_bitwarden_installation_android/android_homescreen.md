---
title: Bitwarden im Play Store suchen
description: Als erstes musst Du den Play Store öffnen, dieser findet sich als Symbol auf Deinem Android-Homescreen. Suche ihn und klicke ihn an. 
image: /img/sichere_logins_02/01_bitwarden_android_homescreen.png
weight: 1
---
