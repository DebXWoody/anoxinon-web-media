---
title: Bitwarden Installation starten 
description: Nach kurzer Zeit ist die Bitwarden fertig herunterladen und man kann die Installation mit einem Doppelklick auf diese beginnen. In dem gezeigten Bildschirmfoto öffnen wir die Bitwarden-Datei direkt aus Firefox, bei anderen Web-Browsern ist der Prozess ähnlich.
image: /img/sichere_logins_02/04_win_bitwarden_installer_file_firefox_e.png
weight: 4
---
