---
title: KeePassXC Passwortdatei anlegen
description: Du wirst aufgefordert, einen Namen und eine Beschreibung für Deine Passwortdatei einzugeben. Was Du dafür wählst, ist Dir überlassen. Mit "Weiter" geht's weiter.
image: /img/sichere_logins_02/keepassxc_db_erstellen_e.png
weight: 7 
---
