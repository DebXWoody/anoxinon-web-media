---
title: KeePassXC Passwortdatei anlegen
description: Hier können Einstellungen zur Verschlüsselung angepasst werden. Für die meisten sind die Standardwerte völlig in Ordnung - klicke einfach "Weiter".
image: /img/sichere_logins_02/keepassxc_db_erstellen_einstellungen_e.png
weight: 9
---
