---
title: KeePassXC installieren 
description: Unter Linux kann man KeePassXC in den meisten Fällen direkt über die Softwareverwaltung installieren. Diese sieht bei jeder Linux-Variante (Distribution) etwas anders aus. Starte also auf gewohntem Weg die Softwareverwaltung und suche nach KeePassXC... 
image: /img/sichere_logins_02/keepassxc_yast_suche.png
weight: 1
---
