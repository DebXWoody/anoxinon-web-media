---
title: Bitwarden ist installiert  
description: Zurück in Bitwarden wird Dir angezeigt, dass AutoFill nun aktiv ist. Nun ist die Installation und die Einrichtung abgeschlossen und das kannst mit Bitwarden auf Deinem iPhone arbeiten!
image: /img/sichere_logins_02/ios_bitwarden_firststart_09_e.PNG
weight: 19
---
