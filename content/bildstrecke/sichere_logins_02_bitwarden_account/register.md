---
title: Bitwarden-Konto erstellen 
description: Dort kannst Du das Formular ausfüllen und absenden - damit ist Dein Konto erstellt. Achte vorallem auf ein sicheres Passwort an dieser Stelle. Mit diesem Passwort kann auf Deinen Bitwarden-Tresor zugegriffen werden, es ist dein Master-Passwort. Wer das kennt, kann auf alle deine gespeicherten Zugangsadaten zugreifen. Du solltest die Kombination aus E-Mail-Adresse und Passwort aber genauso wenig vergessen, denn ohne sie kommst Du nicht mehr an Deine in Bitwarden gespeicherten Zugänge heran - das Kennwort kann nicht zurückgesetzt werden. Wenn Du nicht mehr weißt, wie man sichere und merkbare Passwörter erstellt, schau' doch mal in Teil 1 dieser Artikelserie vorbei. Mit "Fertig" kannst Du die Einstellungen abschließen.
image: /img/sichere_logins_02/02_bitwarden_account_register.png
weight: 2
---
