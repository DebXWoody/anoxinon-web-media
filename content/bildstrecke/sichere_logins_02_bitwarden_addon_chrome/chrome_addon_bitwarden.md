---
title: Bitwarden für Chrome installieren 
description: Nachfolgend kannst Du das Addon einfach mit einem Klick auf "Hinzufügen" installieren.
image: /img/sichere_logins_02/chrome_bitwarden_04_e.png
weight: 4
---
