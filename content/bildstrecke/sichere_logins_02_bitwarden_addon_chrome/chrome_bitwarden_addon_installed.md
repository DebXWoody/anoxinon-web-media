---
title: Bitwarden-Addon ist installiert 
description: Sobald die Installation abgeschlossen ist, öffnet sich automatisch eine Webseite von Bitwarden. Wenn Du möchtest, ließ sie Dir durch, ansonsten kannst Du sie einfach schließen.
image: /img/sichere_logins_02/chrome_bitwarden_07_e.png
weight: 8
---
