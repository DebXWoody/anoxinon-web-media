---
title: Bitwarden im Safari aktivieren 
description: In den Safari-Einstellungen findest Du das Bitwarden-Addon unter "Erweiterungen". Dort musst Du es aktivieren, in dem Du es links neben dem Eintrag anhakst.
image: /img/sichere_logins_02/02_safari_einstellungen.png
weight: 2
---
