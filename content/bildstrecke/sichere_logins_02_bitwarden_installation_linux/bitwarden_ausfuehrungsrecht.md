---
title: Bitwarden Programmdatei 
description: Im Reiter "Berechtigungen" kann man nun "Ausführbar" anwählen, wie im vorherigen Schritt kann das Aussehen und die genaue Bezeichnung je nach Linux-Variante variieren, sinngemäß sollte die Bedeutung aber erkennbar sein. Das Ausführungs-Recht ist notwendig, damit wir Bitwarden folgend starten können.
image: /img/sichere_logins_02/bitwarden_ausführungsrecht_e.png
weight: 6
---
