---
title: KeePassXC installieren 
description: Außerdem kannst Du einige Einstellungen für die Installation anpassen. Empfehlenswert ist es, sich ein Symbol auf dem Desktop anlegen zu lassen, dafür einfach das entsprechende Kästchen anhaken und mit "Next" geht's dann weiter. 
image: /img/sichere_logins_02/08_win_keepassxc_wizard_properties_e.png
weight: 8
---
