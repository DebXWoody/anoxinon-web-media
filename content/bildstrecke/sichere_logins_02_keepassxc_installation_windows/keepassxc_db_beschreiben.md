---
title: KeePassXC Passwortdatei anlegen
description: Du wirst aufgefordert, einen Namen und eine Beschreibung für Deine Passwortdatei einzugeben. Was Du dafür wählst, ist Dir überlassen. Mit "Weiter" geht's weiter.
image: /img/sichere_logins_02/15_win_keepassxc_db_erstellen_name_e.png
weight: 15
---
