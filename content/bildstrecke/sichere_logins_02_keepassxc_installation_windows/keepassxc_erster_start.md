---
title: KeePassXC erster Start 
description: Beim ersten Start fragt KeePassXC noch, ob automatisch nach Updates geschaut werden soll. Das ist zu empfehlen.
image: /img/sichere_logins_02/13_win_keepassxc_first_launch_autoupdates_e.png
weight: 13
---
