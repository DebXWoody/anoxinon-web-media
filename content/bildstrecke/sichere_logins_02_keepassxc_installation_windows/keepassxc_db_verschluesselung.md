---
title: KeePassXC Passwortdatei anlegen
description: Hier können Einstellungen zur Verschlüsselung angepasst werden. Für die meisten sind die Standardwerte völlig in Ordnung - klicke einfach "Weiter".
image: /img/sichere_logins_02/16_win_keepassxc_db_erstellen_verschluesselung_e.png
weight: 17
---
