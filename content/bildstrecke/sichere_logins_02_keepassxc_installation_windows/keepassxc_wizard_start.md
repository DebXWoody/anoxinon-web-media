---
title: KeePassXC installieren 
description: Du kannst nun mit Klick auf "Next" die Installation beginnen.
image: /img/sichere_logins_02/06_win_keepassxc_wizard_e.png
weight: 6
---
