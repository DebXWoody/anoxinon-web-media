---
title: KeePassXC Passwortdatei anlegen
description: Abschließend musst Du einen Speicherort und einen Namen für die Passwortdatei wählen. Das ist völlig Dir überlassen - ABER Du darfst die Datei keinesfalls verlieren, denn dort und nur dort sind alle Deine Passwörter gespeichert. Du brauchst sowohl die Datei als auch das von Dir vergebene Passwort, um Deinen Passwortmanager zu entsperren. Idealerweise kopierst Du die Passwortdatei also regelmäßig auf einen extra USB-Stick oder ähnliches. 
image: /img/sichere_logins_02/18_win_keepassxc_db_erstellen_speicherort_e.png
weight: 19
---
