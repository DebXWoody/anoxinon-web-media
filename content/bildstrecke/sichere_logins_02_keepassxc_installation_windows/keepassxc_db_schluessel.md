---
title: KeePassXC Passwortdatei anlegen
description: Nun wirst Du aufgefordert ein Passwort einzugeben. Wähle hier unbedingt ein sicheres Passwort - dieses Passwort brauchst Du um auf den Passwortmanager zuzugreifen, es schützt alle darin gespeicherten Zugangsdaten vor unberechtigtem Zugriff! Genauso wenig solltest Du es vergessen, ohne es gibt es KEINE Möglichkeit mehr, den Passwortmanager zu entperren. Wenn Du nicht mehr weißt, wie man sichere und merkbare Passwörter erstellt, schau' doch mal in Teil 1 dieser Artikelserie vorbei. Mit "Fertig" kannst Du die Einstellungen abschließen. 
image: /img/sichere_logins_02/17_win_keepassxc_db_erstellen_schluessel_e.png
weight: 18
---
